<!DOCTYPE html>
<html lang="cn" >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>用户管理表格</title>

    <!-- jqGrid组件基础样式包-必要 -->
    <link href="${ctx!}/static/jqgrid/css/ui.jqgrid.css" type="text/css" media="screen" rel="stylesheet"/>
    <link href="${ctx!}/static/jqgrid/css/jquery-ui.css" type="text/css" media="screen" rel="stylesheet"/>
    <link href="${ctx!}/static/css/global.css" type="text/css" media="screen" rel="stylesheet"/>
    <link href="${ctx!}/static/plugins/font-awesome/css/font-awesome.min.css" type="text/css" media="screen"
          rel="stylesheet"/>
    <link href="${ctx!}/static/layui/css/layui.css" type="text/css" media="screen" rel="stylesheet"/>


</head>
<body layadmin-themealias="default" style="background-color:#F3F3F3">
    <div class="layui-fluid">
        <div class="layui-card" style="margin-top: 15px;padding-top: 10px">
            <div class="layui-card-header" style="height: auto">
                <form class="layui-form layui-form-pane" >
                    <div class="layui-row">
                        <div class="layui-col-md6">
                            <div class="layui-form-item">
                                <label class="layui-form-label">用户名</label>
                                <div class="layui-input-block">
                                    <input type="text" name="username" placeholder="请输入" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-md6">
                            <div class="layui-form-item">
                                <label class="layui-form-label">邮箱</label>
                                <div class="layui-input-block">
                                    <input type="text" name="email" placeholder="请输入" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-row">
                        <div class="layui-col-md6">
                            <div class="layui-form-item">
                                <label class="layui-form-label">手机号</label>
                                <div class="layui-input-block">
                                    <input type="text" name="phone" placeholder="请输入" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-md6">
                            <div class="layui-form-item">
                                <label class="layui-form-label">用户状态</label>
                                <div class="layui-input-block">
                                    <input type="text" name="status" placeholder="请输入" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-row">
                        <div class="layui-col-md-offset10">
                            <button class="layui-btn layui-btn-sm" lay-submit lay-filter="searchForm">查询</button>
                            <button class="layui-btn layui-btn-sm" type="reset" >重置</button>
                        </div>
                    </div>
                </form>

            </div>
            <div class="layui-card-body">
                <div class="layui-row">
                    <div  class="layui-col-md4">
                        <button id="add" class="layui-btn layui-btn-sm">新增</button>
                        <button id="modify" class="layui-btn layui-btn-sm">修改</button>
                        <button id="del" class="layui-btn layui-btn-sm">删除</button>
                        <button id="detail" class="layui-btn layui-btn-sm">查看</button>
                    </div>
                </div>
                <div>
                    <table id="list2" class="layui-table" lay-size="sm" lay-filter="userTable"></table>
                    <div id="pager2"></div>
                </div>
            </div>
        </div>
    </div>
<#--↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑为用户设置角色↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑-->
<!-- 在jqgrid/js/i18n下还有其他的多语言包，可以尝试更换看效果 -->
<script src="${ctx!}/static/layui/layui.js" type="text/javascript"></script>
<script type="text/javascript">


    layui.use(['layer', 'form', 'layedit', 'laydate','table'], function () {
        var layer = layui.layer,
                layedit = layui.layedit,
                laydate = layui.laydate,
                $ = layui.$,
                table = layui.table,
                form = layui.form;


        //第一个实例
        table.render({
            id:"list2",
            elem: '#list2',
            url: '${ctx!}/tUser/list', //数据接口
            cellMinWidth: 100,
            page: true, //开启分页
            cols: [[ //表头
                {field: 'id', title: 'ID',sort: false, fixed: 'left'},
                {field: 'username', title: '用户名',},
                {field: 'password', title: '密码',  sort: true},
                {field: 'trueName', title: '真实姓名'},
                {field: 'email', title: '邮箱'},
                {field: 'phone', title: '电话', sort: true},
                {field: 'status', title: '状态', sort: true},
                {field: 'creater', title: '创建人'},
                {field: 'createTime', title: '创建时间',  sort: true}
            ]],
            parseData: function(res){ //res 即为原始返回的数据
                return {
                    "code": res.errcode==200?0:500, //解析接口状态
                    "msg": res.errmsg, //解析提示文本
                    "count": res.data.total, //解析数据长度
                    "data": res.data.list //解析数据列表
                };
            }
        });

        //监听提交
        form.on('submit(searchForm)', function(data){
            table.reload('list2', {
                url: '${ctx!}/tUser/list',
                where: data.field //设定异步数据接口的额外参数
                //,height: 300
            });
            return false;
        });

        //新增按钮
        $("#add").click(function(){
            layer.open({
                title:"用户新增",
                type: 2,
                content:'${ctx!}/tUser/toInsert',
                area: ['80%', '80%'],
                offset: 'auto',//尺寸
                //btn: ['按钮一', '按钮二'],
                btnAlign: 'r',//按钮居右显示
                shade: [0.8, '#393D49'],//遮罩层颜色
                anim: 6,//弹出动画
                maxmin:true,//是否显示最大化按钮
                resize:false,//是否可拉伸
                yes:function(index, layero){

                },
                btn2: function(index, layero){
                    //按钮【按钮二】的回调

                    //return false 开启该代码可禁止点击该按钮关闭
                },
                cancel: function(){
                    //右上角关闭回调

                    //return false 开启该代码可禁止点击该按钮关闭
                }
            })

        });

    });
</script>
</body>
</html>