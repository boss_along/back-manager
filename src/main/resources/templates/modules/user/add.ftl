<!DOCTYPE html>
<html lang="cn" >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>用户管理表格</title>

    <!-- jqGrid组件基础样式包-必要 -->
    <link href="${ctx!}/static/jqgrid/css/ui.jqgrid.css" type="text/css" media="screen" rel="stylesheet"/>
    <link href="${ctx!}/static/jqgrid/css/jquery-ui.css" type="text/css" media="screen" rel="stylesheet"/>
    <link href="${ctx!}/static/css/global.css" type="text/css" media="screen" rel="stylesheet"/>
    <link href="${ctx!}/static/plugins/font-awesome/css/font-awesome.min.css" type="text/css" media="screen"
          rel="stylesheet"/>
    <link href="${ctx!}/static/layui/css/layui.css" type="text/css" media="screen" rel="stylesheet"/>


</head>
<body layadmin-themealias="default" style="background-color:#F3F3F3">
<div class="layui-fluid">
    <div class="layui-card" style="margin-top: 15px;padding-top: 10px">
        <div class="layui-card-header" style="height: auto">
            新增
        </div>
        <div class="layui-card-body">
            <form class="layui-form layui-form-pane" >
                <div class="layui-row">
                    <div class="layui-col-md6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">用户名</label>
                            <div class="layui-input-block">
                                <input type="text" name="" placeholder="请输入" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-md6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">邮箱</label>
                            <div class="layui-input-block">
                                <input type="text" name="" placeholder="请输入" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-row">
                    <div class="layui-col-md6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">手机号</label>
                            <div class="layui-input-block">
                                <input type="text" name="" placeholder="请输入" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-md6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">用户状态</label>
                            <div class="layui-input-block">
                                <input type="text" name="" placeholder="请输入" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-row">
                    <div  class="layui-col-md-offset10">
                        <button class="layui-btn layui-btn-sm">查询</button>
                        <button class="layui-btn layui-btn-sm">重置</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="${ctx!}/static/layui/layui.js" type="text/javascript"></script>
<script type="text/javascript">


    layui.use(['layer', 'form', 'layedit', 'laydate'], function () {
        var layer = layui.layer,
                layedit = layui.layedit,
                laydate = layui.laydate,
                $ = layui.$,
                form = layui.form;



    });
</script>
</body>
</html>