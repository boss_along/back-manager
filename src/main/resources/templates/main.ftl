<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台管理系统</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="${ctx!}/static/logo.png">
    <link rel="stylesheet" href="${ctx!}/static/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="${ctx!}/static/css/index.css" media="all" />
    <style>
        .rightmenu { position: absolute; width: 80px; z-index: 9999; display: none; background-color: #fff; padding: 2px; color: #333; border: 1px solid #eee; border-radius: 2px; cursor: pointer; }
        .rightmenu li { text-align: center; display: block; height: 25px; line-height: 25px; }
        .rightmenu li:hover { background-color: #666; color: #fff; }
    </style>

</head>
<body class="layui-layout-body" layadmin-themealias="default">
<div class="layui-layout layui-layout-admin">
    <!-- 顶部 -->
    <div class="layui-header">
        <div class="layui-main">
            <a href="${ctx!}/welcome" class="logo">ZJT.Blog</a>
            <!-- 显示/隐藏菜单 -->
            <a href="javascript:;" class="seraph hideMenu icon-caidan"></a>
            <!-- 顶级菜单 -->
            <ul class="layui-nav topLevelMenus" pc>

            </ul>
            <!-- 顶部右侧菜单 -->
            <ul class="layui-nav top_menu">
                <li class="layui-nav-item" pc>
                    <a href="javascript:;" class="clearCache"><i class="layui-icon" data-icon="&#xe640;">&#xe640;</i><cite>清除缓存</cite><span class="layui-badge-dot"></span></a>
                </li>
                <li class="layui-nav-item lockcms" pc>
                    <a href="javascript:;"><i class="seraph icon-lock"></i><cite>锁屏</cite></a>
                </li>
                <li class="layui-nav-item" id="userInfo">
                    <a href="javascript:;"><img src="${ctx!}/static/logo.png" class="layui-nav-img userAvatar" width="35" height="35"><cite class="adminName"><div id="userInfoId"></div></cite></a>
                    <dl class="layui-nav-child">
                        <dd pc><a href="javascript:;" class="changeSkin"><i class="layui-icon">&#xe61b;</i><cite>更换皮肤</cite></a></dd>
                        <dd><a href="${ctx!}/user/logout" class="signOut"><i class="seraph icon-tuichu"></i><cite>退出</cite></a></dd>
                    </dl>
                </li>
            </ul>
        </div>
    </div>
    <!-- 左侧导航 -->
    <div class="layui-side layui-bg-black">
        <div class="user-photo">
            <a class="img" title="我的头像" ><img src="${ctx!}/static/logo.png" class="userAvatar"></a>
            <p>你好！<span class="userName">${loginUser.trueName!}</span>, 欢迎登录</p>
        </div>

        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航）-->
        </div>
    </div>
    <!-- 右侧内容 -->
    <div class="layui-body layui-form">
        <div class="layui-tab" lay-allowClose="true" lay-filter="bodyTab" id="top_tabs_box">

            <ul class="layui-tab-title" id="top_tabs">
                <li class="layui-this" lay-id=""><i class="layui-icon">&#xe68e;</i> <cite>后台首页</cite></li>
            </ul>

            <ul class="rightmenu">
                <li data-type="closeOther">关闭其他</li>
                <li data-type="closeall">关闭所有</li>
            </ul>

            <!-- 中间iframe框 -->
            <div class="layui-tab-content clildFrame">
                <div class="layui-tab-item layui-show">
                    <iframe src="${ctx!}/inner" style="margin-top: 10px"></iframe>
                </div>
            </div>

        </div>
    </div>
    <!-- 底部 -->
    <div class="layui-footer footer">
        <p><span>copyright @2018 ZJT</span></p>
    </div>
</div>

<!-- 移动导航 -->
<div class="site-tree-mobile"><i class="layui-icon">&#xe602;</i></div>
<div class="site-mobile-shade"></div>

<script type="text/javascript" src="${ctx!}/static/layui/layui.js"></script>
<script type="text/javascript" src="${ctx!}/static/js/cache.js"></script>


<#--把index.js提到页面中，为了使用freemarker-->
<script type="text/javascript" >

    //JavaScript代码区域
    layui.use('element', function() {
        var element = layui.element;
        var $ = layui.$;

        getMenuData(-1);


        $('.topLevelMenus').on('click', '.layui-nav-item', function () {//事件委托，请看下方注释
            let pid = $(this).children("input").val();
            getMenuData(pid);
        });

        /*$(".topLevelMenus .layui-nav-item").click(function () {
            let pid = $(this).children("input").val();
            getMenuData(pid);
        });*/

        //获取菜单数据
        function getMenuData(pid) {
            $.ajax({
                url: '${ctx}/tMenu/selectByPid/' + pid,
                data: '',
                dataType: 'json',
                method: 'GET',
                async: true,
                success: function (result) {
                    if (result.success = true) {
                        dataStr = result.data;
                        if ('-1' == pid) {
                            assembleTopMenu(dataStr);
                            getMenuData(dataStr[0].id);
                        } else {
                            assembleLeftMenu(dataStr);
                        }
                    } else {
                        layer.msg(result.errmsg, {icon: 2, time: 1200});
                    }
                }
            })
        }

        //组装顶部菜单
        function assembleTopMenu(data) {

            if (typeof (data) == "string") {
                data = JSON.parse(data);
            }

            var navHtml = "";

            for (var i = 0; i < data.length; i++) {
                var menu = data[i];
                if (0 == i) {
                    navHtml += "<li class=\"layui-nav-item layui-this\" data-menu=\"" + menu.name + "\">";
                } else {
                    navHtml += "<li class=\"layui-nav-item\" data-menu=\"" + menu.name + "\">";
                }
                navHtml += "<a href=\"javascript:;\">";
                navHtml += "<i class=\"layui-icon\" data-icon=\"" + menu.icon + "\">" + menu.icon + "</i>";
                navHtml += "<cite>" + menu.name + "</cite>";
                navHtml += "</a>";
                navHtml += "<input type=\"hidden\" value=\"" + menu.id + "\">";
                navHtml += "</li>";
            }
            $(".topLevelMenus").empty();
            $(".topLevelMenus").append(navHtml);
            element.render(".topLevelMenus");
        }

        //组装左侧菜单
        function assembleLeftMenu(data) {

            if (typeof (data) == "string") {
                data = JSON.parse(data);
            }

            var navHtml = "<ul class=\"layui-nav layui-nav-tree\">";

            for (var i = 0; i < data.length; i++) {

                var menu = data[i];

                if (menu.isLeaf == 1) {
                    navHtml += "<li class=\"layui-nav-item leafMenu\" id=\""+menu.id+"\"  src=\""+menu.url+"\" name=\""+menu.name+"\">";
                } else {
                    navHtml += "<li class=\"layui-nav-item layui-nav-itemed\">";
                }
                navHtml += "<a class=\"\" href=\"javascript:;\">" + menu.name + "</a>";
                if (menu.isLeaf != 1) {
                    navHtml += "<dl class=\"layui-nav-child\">";
                    navHtml += "</dl>";
                }
                navHtml += "</li>";
            }
            navHtml += "</ul>";
            $(".layui-side-scroll").empty();
            $(".layui-side-scroll").append(navHtml);
            element.render(".layui-side-scroll");
        }

        //隐藏左侧导航
        $(".hideMenu").click(function () {
            $(".layui-layout-admin").toggleClass("showMenu");
        });

        //左侧菜单点击事件
        $(".layui-side-scroll").on('click', '.leafMenu', function () {
            var url = $(this).attr('src');
            var id = $(this).attr('id');
            var name = $(this).attr('name');
            addTab(url,id,name);
        })


        //新增tab页
        function addTab(url,id,name) {

            if(isMaxTabs()){return;}

            if(isExist(url,id)){ return;}

            element.tabAdd('bodyTab', {
                title: name,
                content: '<iframe data-frameid="'+id+'" scrolling="auto" frameborder="0" style="margin-top: 10px" src="'+url+'"></iframe>', //支持传入html
                id: id
            });
            element.tabChange('bodyTab', id);
        }

        //是否超过最大tab页个数
        function isMaxTabs(){
            let tabLength = $(".layui-tab-title li").length;
            if(tabLength>=3){
                layer.msg('已超过tab页最大个数', {icon: 2, time: 1200});
                return true;
            }
        }

        //判断当前tab页是否已经打开
        function isExist(url,id){
            var flag = false;
            $(".layui-tab-title li").each(function () {
                let tabId = $(this).attr('lay-id');
                if(id == tabId){
                    element.tabChange('bodyTab', id);
                    flag =  true;
                }
            });
            return flag;
        }

        //tab页鼠标右击事件
        $('.layui-tab-title').unbind("mousedown").on('contextmenu','li', function (e) {
            var index = $(".layui-tab-title li").index(this);
            index = index+1;
            var popupmenu = $(".rightmenu");
            l = 112*index +24.89;
            t = $(".layui-tab-title").css('height');
            popupmenu.css({ left: l, top: t }).show();
            return false;
        });


        //关闭所有的Tab页
        function closeAllTab(){
            $(".layui-tab-title li").each(function () {
                let tabId = $(this).attr('lay-id');
                element.tabDelete('bodyTab', tabId);
            });
        }


        //就需要给右键添加功能
        $(".rightmenu li").click(function () {
            debugger;
            if ($(this).attr("data-type") == "closeOther") {//关闭其他
                alert('关闭其他');
            } else if ($(this).attr("data-type") == "closeall") {//关闭所有
                closeAllTab();
            }
            $('.rightmenu').hide();
        });

    });




    /*//手机设备的简单适配
    $('.site-tree-mobile').on('click', function(){
        $('body').addClass('site-mobile');
    });
    $('.site-mobile-shade').on('click', function(){
        $('body').removeClass('site-mobile');
    });*/


</script>


</body>
</html>