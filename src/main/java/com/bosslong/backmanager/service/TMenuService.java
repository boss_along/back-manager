package com.bosslong.backmanager.service;

import com.bosslong.backmanager.entity.TMenuDTO;

import java.util.List;

/**
 * (TMenu)表服务接口
 *
 * @author makejava
 * @since 2019-01-10 17:30:46
 */
public interface TMenuService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TMenuDTO queryById(String id);


    /**
     * 根据角色Id 查询菜单
     * @param roleId
     * @return
     * @throws Exception
     */
    List<TMenuDTO> queryByRoleId(String roleId) throws Exception;

    /**
     * 根据角色ID查询用户菜单
     * @param roleIds
     * @return
     * @throws Exception
     */
    List<TMenuDTO> queryByRoleIds(List<String> roleIds) throws Exception;

    /**
     * 根据角色ID，和菜单等级查询
     * @param roleIds
     * @param pId
     * @return
     * @throws Exception
     */
    List<TMenuDTO> queryByRoleIdsAndPid(List<String> roleIds,String pId) throws Exception;
    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<TMenuDTO> queryAllByLimit(int offset, int limit);

    /**
     * 根据上级菜单Id 查询菜单
     * @param pId
     * @return
     * @throws Exception
     */
    List<TMenuDTO> queryByPid(String pId) throws Exception;

    /**
     * 新增数据
     *
     * @param tMenu 实例对象
     * @return 实例对象
     */
    TMenuDTO insert(TMenuDTO tMenu);

    /**
     * 修改数据
     *
     * @param tMenu 实例对象
     * @return 实例对象
     */
    TMenuDTO update(TMenuDTO tMenu);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

}