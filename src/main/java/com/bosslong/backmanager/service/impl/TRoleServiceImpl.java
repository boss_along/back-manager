package com.bosslong.backmanager.service.impl;

import com.bosslong.backmanager.dao.TRoleDao;
import com.bosslong.backmanager.entity.TRoleDTO;
import com.bosslong.backmanager.service.TRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

/**
 * (TRole)表服务实现类
 *
 * @author makejava
 * @since 2019-01-10 17:30:48
 */
@Service
public class TRoleServiceImpl implements TRoleService {
    @Resource
    private TRoleDao tRoleDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public TRoleDTO queryById(String id) {
        return this.tRoleDao.queryById(id);
    }


    @Override
    public Set<TRoleDTO> queryByUserId(String userId)  throws Exception {
        return tRoleDao.queryByUserId(userId);
    }
    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<TRoleDTO> queryAllByLimit(int offset, int limit) {
        return this.tRoleDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param tRole 实例对象
     * @return 实例对象
     */
    @Override
    public TRoleDTO insert(TRoleDTO tRole) {
        this.tRoleDao.insert(tRole);
        return tRole;
    }

    /**
     * 修改数据
     *
     * @param tRole 实例对象
     * @return 实例对象
     */
    @Override
    public TRoleDTO update(TRoleDTO tRole) {
        this.tRoleDao.update(tRole);
        return this.queryById(tRole.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.tRoleDao.deleteById(id) > 0;
    }
}