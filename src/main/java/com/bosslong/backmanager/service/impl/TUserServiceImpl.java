package com.bosslong.backmanager.service.impl;

import com.bosslong.backmanager.common.utils.UserInfo;
import com.bosslong.backmanager.dao.TUserDao;
import com.bosslong.backmanager.entity.TUserDTO;
import com.bosslong.backmanager.service.TUserService;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * (TUser)表服务实现类
 *
 * @author makejava
 * @since 2019-01-10 17:30:48
 */
@Service
public class TUserServiceImpl implements TUserService {
    @Resource
    private TUserDao tUserDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public TUserDTO queryById(String id) {
        return this.tUserDao.queryById(id);
    }

    @Override
    public TUserDTO queryByUsername(String username) throws Exception {
        return tUserDao.queryByUsername(username);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<TUserDTO> queryAllByLimit(int offset, int limit) {
        return this.tUserDao.queryAllByLimit(offset, limit);
    }

    @Override
    public List<TUserDTO> query4Page(TUserDTO userDTO) throws Exception {
        return tUserDao.query4Page(userDTO);
    }

    /**
     * 新增数据
     *
     * @param tUser 实例对象
     * @return 实例对象
     */
    @Override
    public TUserDTO insert(TUserDTO tUser) {
        String userName = UserInfo.getUserName();

        tUser.setCreater(userName);
        tUser.setCreateTime(new Date());
        tUserDao.insert(tUser);
        return tUser;
    }

    /**
     * 修改数据
     *
     * @param tUser 实例对象
     * @return 实例对象
     */
    @Override
    public TUserDTO update(TUserDTO tUser) {

        String userName = UserInfo.getUserName();
        tUser.setModifer(userName);
        tUser.setModifyTime(new Date());

        tUserDao.update(tUser);
        return tUser;
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.tUserDao.deleteById(id) > 0;
    }
}