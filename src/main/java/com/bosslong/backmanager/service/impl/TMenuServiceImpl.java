package com.bosslong.backmanager.service.impl;

import com.bosslong.backmanager.dao.TMenuDao;
import com.bosslong.backmanager.entity.TMenuDTO;
import com.bosslong.backmanager.service.TMenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (TMenu)表服务实现类
 *
 * @author makejava
 * @since 2019-01-10 17:30:46
 */
@Service
public class TMenuServiceImpl implements TMenuService {
    @Resource
    private TMenuDao tMenuDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public TMenuDTO queryById(String id) {
        return this.tMenuDao.queryById(id);
    }


    @Override
    public List<TMenuDTO> queryByRoleId(String roleId) throws Exception {
        return tMenuDao.queryByRoleId(roleId);
    }

    @Override
    public List<TMenuDTO> queryByRoleIds(List<String> roleIds) throws Exception {
        return tMenuDao.queryByRoleIds(roleIds);
    }

    @Override
    public List<TMenuDTO> queryByRoleIdsAndPid(List<String> roleIds, String pId) throws Exception {
        return tMenuDao.queryByRoleIdsAndPid(roleIds,pId);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<TMenuDTO> queryAllByLimit(int offset, int limit) {
        return this.tMenuDao.queryAllByLimit(offset, limit);
    }

    @Override
    public List<TMenuDTO> queryByPid(String pId) throws Exception {
        return tMenuDao.queryByPid(pId);
    }

    /**
     * 新增数据
     *
     * @param tMenu 实例对象
     * @return 实例对象
     */
    @Override
    public TMenuDTO insert(TMenuDTO tMenu) {
        this.tMenuDao.insert(tMenu);
        return tMenu;
    }

    /**
     * 修改数据
     *
     * @param tMenu 实例对象
     * @return 实例对象
     */
    @Override
    public TMenuDTO update(TMenuDTO tMenu) {
        this.tMenuDao.update(tMenu);
        return this.queryById(tMenu.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.tMenuDao.deleteById(id) > 0;
    }
}