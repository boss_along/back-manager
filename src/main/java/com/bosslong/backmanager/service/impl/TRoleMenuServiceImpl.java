package com.bosslong.backmanager.service.impl;

import com.bosslong.backmanager.dao.TRoleMenuDao;
import com.bosslong.backmanager.entity.TRoleMenuDTO;
import com.bosslong.backmanager.service.TRoleMenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (TRoleMenu)表服务实现类
 *
 * @author makejava
 * @since 2019-01-10 17:30:48
 */
@Service
public class TRoleMenuServiceImpl implements TRoleMenuService {
    @Resource
    private TRoleMenuDao tRoleMenuDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public TRoleMenuDTO queryById(String id) {
        return this.tRoleMenuDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<TRoleMenuDTO> queryAllByLimit(int offset, int limit) {
        return this.tRoleMenuDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param tRoleMenu 实例对象
     * @return 实例对象
     */
    @Override
    public TRoleMenuDTO insert(TRoleMenuDTO tRoleMenu) {
        this.tRoleMenuDao.insert(tRoleMenu);
        return tRoleMenu;
    }

    /**
     * 修改数据
     *
     * @param tRoleMenu 实例对象
     * @return 实例对象
     */
    @Override
    public TRoleMenuDTO update(TRoleMenuDTO tRoleMenu) {
        this.tRoleMenuDao.update(tRoleMenu);
        return this.queryById(tRoleMenu.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.tRoleMenuDao.deleteById(id) > 0;
    }
}