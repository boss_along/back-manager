package com.bosslong.backmanager.service.impl;

import com.bosslong.backmanager.dao.TUserRoleDao;
import com.bosslong.backmanager.entity.TUserRoleDTO;
import com.bosslong.backmanager.service.TUserRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (TUserRole)表服务实现类
 *
 * @author makejava
 * @since 2019-01-10 17:30:48
 */
@Service
public class TUserRoleServiceImpl implements TUserRoleService {
    @Resource
    private TUserRoleDao tUserRoleDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public TUserRoleDTO queryById(String id) {
        return this.tUserRoleDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<TUserRoleDTO> queryAllByLimit(int offset, int limit) {
        return this.tUserRoleDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param tUserRole 实例对象
     * @return 实例对象
     */
    @Override
    public TUserRoleDTO insert(TUserRoleDTO tUserRole) {
        this.tUserRoleDao.insert(tUserRole);
        return tUserRole;
    }

    /**
     * 修改数据
     *
     * @param tUserRole 实例对象
     * @return 实例对象
     */
    @Override
    public TUserRoleDTO update(TUserRoleDTO tUserRole) {
        this.tUserRoleDao.update(tUserRole);
        return this.queryById(tUserRole.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.tUserRoleDao.deleteById(id) > 0;
    }
}