package com.bosslong.backmanager.service;

import com.bosslong.backmanager.entity.TRoleMenuDTO;

import java.util.List;

/**
 * (TRoleMenu)表服务接口
 *
 * @author makejava
 * @since 2019-01-10 17:30:48
 */
public interface TRoleMenuService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TRoleMenuDTO queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<TRoleMenuDTO> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param tRoleMenu 实例对象
     * @return 实例对象
     */
    TRoleMenuDTO insert(TRoleMenuDTO tRoleMenu);

    /**
     * 修改数据
     *
     * @param tRoleMenu 实例对象
     * @return 实例对象
     */
    TRoleMenuDTO update(TRoleMenuDTO tRoleMenu);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

}