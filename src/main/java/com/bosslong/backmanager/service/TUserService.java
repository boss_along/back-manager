package com.bosslong.backmanager.service;

import com.bosslong.backmanager.entity.TUserDTO;

import java.util.List;

/**
 * (TUser)表服务接口
 *
 * @author makejava
 * @since 2019-01-10 17:30:48
 */
public interface TUserService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TUserDTO queryById(String id);

    /**
     * 通过用户名查询用户信息
     * @param username
     * @return
     * @throws Exception
     */
    TUserDTO queryByUsername(String username) throws Exception;

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<TUserDTO> queryAllByLimit(int offset, int limit);

    /**
     * 分页查询
     * @return
     * @throws Exception
     */
    List<TUserDTO> query4Page(TUserDTO userDTO) throws Exception ;

    /**
     * 新增数据
     *
     * @param tUser 实例对象
     * @return 实例对象
     */
    TUserDTO insert(TUserDTO tUser);

    /**
     * 修改数据
     *
     * @param tUser 实例对象
     * @return 实例对象
     */
    TUserDTO update(TUserDTO tUser);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

}