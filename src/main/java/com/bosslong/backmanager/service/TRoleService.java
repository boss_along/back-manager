package com.bosslong.backmanager.service;

import com.bosslong.backmanager.entity.TRoleDTO;

import java.util.List;
import java.util.Set;

/**
 * (TRole)表服务接口
 *
 * @author makejava
 * @since 2019-01-10 17:30:48
 */
public interface TRoleService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TRoleDTO queryById(String id);

    /**
     * 根据用户ID查询角色列表
     * @param userId
     * @return
     */
    Set<TRoleDTO> queryByUserId(String userId) throws Exception;

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<TRoleDTO> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param tRole 实例对象
     * @return 实例对象
     */
    TRoleDTO insert(TRoleDTO tRole);

    /**
     * 修改数据
     *
     * @param tRole 实例对象
     * @return 实例对象
     */
    TRoleDTO update(TRoleDTO tRole);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

}