package com.bosslong.backmanager.dao;

import com.bosslong.backmanager.entity.TRoleDTO;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Set;

/**
 * (TRole)表数据库访问层
 *
 * @author makejava
 * @since 2019-01-10 17:30:48
 */
public interface TRoleDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TRoleDTO queryById(String id);

    /**
     * 根据用户ID查询角色
     * @param userId
     * @return
     * @throws Exception
     */
    Set<TRoleDTO> queryByUserId(@Param("userId") String userId) throws Exception;

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<TRoleDTO> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param tRole 实例对象
     * @return 对象列表
     */
    List<TRoleDTO> queryAll(TRoleDTO tRole);

    /**
     * 新增数据
     *
     * @param tRole 实例对象
     * @return 影响行数
     */
    int insert(TRoleDTO tRole);

    /**
     * 修改数据
     *
     * @param tRole 实例对象
     * @return 影响行数
     */
    int update(TRoleDTO tRole);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

}