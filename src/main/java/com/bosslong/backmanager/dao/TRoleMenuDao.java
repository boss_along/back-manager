package com.bosslong.backmanager.dao;

import com.bosslong.backmanager.entity.TRoleMenuDTO;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (TRoleMenu)表数据库访问层
 *
 * @author makejava
 * @since 2019-01-10 17:30:48
 */
public interface TRoleMenuDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TRoleMenuDTO queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<TRoleMenuDTO> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param tRoleMenu 实例对象
     * @return 对象列表
     */
    List<TRoleMenuDTO> queryAll(TRoleMenuDTO tRoleMenu);

    /**
     * 新增数据
     *
     * @param tRoleMenu 实例对象
     * @return 影响行数
     */
    int insert(TRoleMenuDTO tRoleMenu);

    /**
     * 修改数据
     *
     * @param tRoleMenu 实例对象
     * @return 影响行数
     */
    int update(TRoleMenuDTO tRoleMenu);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

}