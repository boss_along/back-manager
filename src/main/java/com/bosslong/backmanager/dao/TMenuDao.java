package com.bosslong.backmanager.dao;

import com.bosslong.backmanager.entity.TMenuDTO;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (TMenu)表数据库访问层
 *
 * @author makejava
 * @since 2019-01-10 17:30:45
 */
public interface TMenuDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TMenuDTO queryById(String id);


    /**
     * 根据角色ID查询菜单
     * @param roleId
     * @return
     * @throws Exception
     */
    List<TMenuDTO> queryByRoleId(String roleId) throws Exception;


    /**
     * 根据角色ID查询菜单
     * @param roleIds
     * @return
     * @throws Exception
     */
    List<TMenuDTO> queryByRoleIds(@Param("roleIds") List<String> roleIds) throws Exception;

    /**
     * 根据角色ID，和菜单等级查询
     * @param roleIds
     * @param pId
     * @return
     * @throws Exception
     */
    List<TMenuDTO> queryByRoleIdsAndPid(@Param("roleIds") List<String> roleIds, @Param("pId") String pId) throws Exception;

    /**
     * 根据pId查询菜单
     * @param pId
     * @return
     * @throws Exception
     */
    List<TMenuDTO> queryByPid(String pId) throws Exception;
    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<TMenuDTO> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param tMenu 实例对象
     * @return 对象列表
     */
    List<TMenuDTO> queryAll(TMenuDTO tMenu);

    /**
     * 新增数据
     *
     * @param tMenu 实例对象
     * @return 影响行数
     */
    int insert(TMenuDTO tMenu);

    /**
     * 修改数据
     *
     * @param tMenu 实例对象
     * @return 影响行数
     */
    int update(TMenuDTO tMenu);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

}