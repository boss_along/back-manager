package com.bosslong.backmanager.common.thread;

import java.util.Objects;
import java.util.concurrent.*;

/**
 * @author Thinkpad
 * @Title: ThreadPool
 * @ProjectName back-manager
 * @Description: TODO
 * @date 2019/1/2810:46
 * @Version: 1.0
 */
public class ThreadPool {

    private static final int minPoolSize = 1;

    private static final int maxPoolSize = Runtime.getRuntime().availableProcessors();

    private static ExecutorService executorService;

    /**
     * 构造私有
     */
    private ThreadPool(){}

    /**
     *
     * @return ExecutorService
     */
    public static ExecutorService newInstance(){

        if(Objects.isNull(executorService)){
            synchronized (ThreadPool.class){
                if(Objects.isNull(executorService)){
                    executorService = new ThreadPoolExecutor(minPoolSize, maxPoolSize,
                            0L, TimeUnit.MILLISECONDS,
                            new LinkedBlockingQueue<Runnable>(1024));
                }
            }
        }
        return executorService;
    }
}
