package com.bosslong.backmanager.common.config;

import com.bosslong.backmanager.common.interceptor.AuthorTokenInterceptor;
import com.bosslong.backmanager.common.interceptor.CommonInterceptor;
import com.bosslong.backmanager.common.interceptor.IPBlackInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class InitWebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private AuthorTokenInterceptor authorTokenInterceptor;

    @Autowired
    private IPBlackInterceptor ipBlackInterceptor;

    @Autowired
    private CommonInterceptor commonInterceptor;

    public InitWebMvcConfig() {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        InterceptorRegistration interceptorRegistration = registry.addInterceptor(authorTokenInterceptor);

        List<String> pathPatten = new ArrayList<>();
        pathPatten.add("/login");
        pathPatten.add("/tUser/login");
        pathPatten.add("/drawImage");
        pathPatten.add("/static/**");
        pathPatten.add("/");
        interceptorRegistration.addPathPatterns("/**");

        interceptorRegistration.excludePathPatterns(pathPatten);


        InterceptorRegistration interceptorRegistration1 = registry.addInterceptor(commonInterceptor);

        interceptorRegistration1.addPathPatterns("/**");
        //super.addInterceptors(registry);

    }
}
