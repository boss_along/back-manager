package com.bosslong.backmanager.common.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: boss_along
 * @Date: 2019/1/16 18:37
 * @Description:
 */
@Data
public class Result implements Serializable {

    private boolean success;

    private Integer errcode;

    private String errmsg;

    private Object data;

    private Result(boolean success,Integer errcode,String errmsg,Object data){
        this.success = success;
        this.errcode = errcode;
        this.errmsg = errmsg;
        this.data = data;
    }

    /**
     * 有返回数据集
     * @param data 返回数据集
     * @return
     */
    public static Result ok(Object data){
       return new Result(true,200,null,data);
    }

    /**
     * 无结果集
     * @param errcode 返回码
     * @param errmsg 返回信息
     * @return
     */
    public static Result ok(Integer errcode,String errmsg){
        return new Result(true,errcode,errmsg,null);
    }

    /**
     * 无返回数据集
     * @return
     */
    public static Result ok(){
        return new Result(true,200,null,null);
    }

    /**
     * 错误
     * @param errcode 错误码
     * @param errmesg 错误信息
     * @return
     */
    public static Result error(Integer errcode,String errmesg){
        return new Result(false,errcode,errmesg,null);
    }
}
