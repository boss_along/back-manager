package com.bosslong.backmanager.common.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 全局异常处理器
 * 发生系统错误时捕获返回出错页面
 * @Author: boss_along
 * @Date: 2019/1/11 10:05
 * @Description:
 */
@ControllerAdvice
@Slf4j
public class GlobleException {

    @ExceptionHandler(Exception.class)
    public ModelAndView resolveException(HttpServletRequest request,Exception e) throws Exception{

        StringBuffer requestURL = request.getRequestURL();

        log.info(">>>>>>>>>>>>>>>>>全局异常处理器执行中...<<<<<<<<<<<<<<<<<<");
        log.info(">>>>>>>>>>>>>>>>>异常URL:"+requestURL+"<<<<<<<<<<<<<<<<<<");
        log.info(">>>>>>>>>>>>>>>>>异常原因:<<<<<<<<<<<<<<<<<<",e);

        ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("exception", e);
        modelAndView.addObject("url", request.getRequestURL());

        modelAndView.setViewName("error/errordeal");

        if(e instanceof org.apache.shiro.authz.UnauthorizedException){
            modelAndView.setViewName("error/unauthorized");
        }
        if(e instanceof org.apache.shiro.authz.AuthorizationException){
            modelAndView.setViewName("error/unauthorized");
        }

        return modelAndView;
    }
}
