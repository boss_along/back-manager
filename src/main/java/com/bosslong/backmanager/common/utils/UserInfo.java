package com.bosslong.backmanager.common.utils;

import com.bosslong.backmanager.entity.TUserDTO;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.BeanUtils;

/**
 * @author Thinkpad
 * @Title: UserInfo
 * @ProjectName back-manager
 * @Description: TODO
 * @date 2019/1/2811:20
 * @Version: 1.0
 */
public class UserInfo {

    private UserInfo(){}

    public static TUserDTO getUserInfo(){
        TUserDTO user = null;
        try{
            user = (TUserDTO)SecurityUtils.getSubject().getPrincipal();
        }catch (ClassCastException e){
            Object principal = SecurityUtils.getSubject().getPrincipal();
            user = new TUserDTO();
            BeanUtils.copyProperties(principal,user);
        }
        return user;
    }

    public static String getUserName(){
        return getUserInfo().getUsername();
    }


}
