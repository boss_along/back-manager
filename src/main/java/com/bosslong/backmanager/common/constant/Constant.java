package com.bosslong.backmanager.common.constant;

/**
 * @Author: boss_along
 * @Date: 2019/1/11 12:37
 * @Description:
 */
public class Constant {

    /** 登录用户token的key **/
    public static final String AUTHOR_TOKEN = "token";
}
