package com.bosslong.backmanager.common.constant;


public class Constant_Error {

    /** 系统错误码 **/
    public static final Integer SYS_ERRCODE_SERVER = 500;
    /** 系统错误返回信息 **/
    public static final String SYS_ERRCODE_SERVER_MSGS = "系统错误，请联系管理员！";

    /** 系统错误码 **/
    public static final Integer SYS_ERRCODE_USERNAMEORPASSWORD = 20001;
    /** 系统错误返回信息 **/
    public static final String SYS_ERRCODE_USERNAMEORPASSWORD_MSGS = "用户名或密码错误！";
}
