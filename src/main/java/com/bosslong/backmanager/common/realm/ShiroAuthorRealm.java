package com.bosslong.backmanager.common.realm;

import com.bosslong.backmanager.entity.TMenuDTO;
import com.bosslong.backmanager.entity.TRoleDTO;
import com.bosslong.backmanager.entity.TUserDTO;
import com.bosslong.backmanager.service.TMenuService;
import com.bosslong.backmanager.service.TRoleService;
import com.bosslong.backmanager.service.TUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author: boss_along
 * @Date: 2019/1/11 16:08
 * @Description:
 */
@Slf4j
public class ShiroAuthorRealm extends AuthorizingRealm {

    @Autowired
    private TUserService tUserService;

    @Autowired
    private TRoleService tRoleService;

    @Autowired
    private TMenuService tMenuService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        log.info(">>>>>>>>>>>>>>>>>> 执行 Shiro 权限获取 <<<<<<<<<<<<<<<<<<<<`");
        Object principal = principalCollection.getPrimaryPrincipal();
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        if (principal instanceof TUserDTO) {
            TUserDTO userLogin = (TUserDTO) principal;

            Set<TRoleDTO> roles = null;
            try {
                roles = tRoleService.queryByUserId(userLogin.getId());
                List<String> roleList = new ArrayList<>(10);
                for (TRoleDTO role: roles) {
                    roleList.add(role.getName());
                    List<TMenuDTO> tMenus = tMenuService.queryByRoleId(role.getId());
                    for (TMenuDTO tMenu:tMenus) {
                        authorizationInfo.addStringPermission(tMenu.getName());
                    }

                }
                authorizationInfo.addRoles(roleList);

                return authorizationInfo;
            } catch (Exception e) {
                log.error("com.bosslong.backmanager.common.realm.ShiroAuthorRealm.doGetAuthorizationInfo add roles and permissions error...",e);
            }
        }
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        //通过表单接收的用户名，调用currentUser.login的时候执行
        String username = token.getUsername();
        if(StringUtils.isNotBlank(username)){
            TUserDTO tUser = null;
            try {
                tUser = tUserService.queryByUsername(username);

                if(Objects.isNull(tUser)) {return null;}

                setRoles(tUser);

                setMenus(tUser);

                return new SimpleAuthenticationInfo(tUser, token.getPassword(), getName());

            } catch (Exception e) {
                log.error("com.bosslong.backmanager.common.realm.ShiroAuthorRealm.doGetAuthenticationInfo query user by username error...",e);
                return null;
            }
        }
        return null;
    }


    /**
     * 组装用户角色
     * @param tUser
     * @throws Exception
     */
    private void setRoles(TUserDTO tUser) throws Exception{

        Set<TRoleDTO> roles = tRoleService.queryByUserId(tUser.getId());

        tUser.setRoles(new ArrayList<>(roles));
    }

    /**
     * 组装用户菜单
     * @param tUser
     */
    private void setMenus(TUserDTO tUser) throws Exception{

        List<TRoleDTO> roles = tUser.getRoles();

        Assert.notEmpty(roles,"获取用户角色异常...");

        Set<String> roleIds = roles.stream().collect(Collectors.groupingBy(TRoleDTO::getId)).keySet();


        List<TMenuDTO> tMenuDTOS = tMenuService.queryByRoleIds(new ArrayList<String>(roleIds));

        Map<Integer, List<TMenuDTO>> map = tMenuDTOS.stream().collect(Collectors.groupingBy(TMenuDTO::getState));

        Set<Integer> states = map.keySet();

        List<Integer> stateList = new ArrayList<>(states);

        Collections.sort(stateList, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        });

        Integer integer = stateList.get(0);
        List<TMenuDTO> pmenus = map.get(integer);

        for (TMenuDTO menu:pmenus) {
            menu.setChild(getChild(tMenuDTOS, menu.getId()));
        }

        tUser.setMenus(pmenus);
    }

    /**
     * 递归组装树形菜单
     * @param menus
     * @param id
     * @return
     */
    private List<TMenuDTO> getChild(List<TMenuDTO> menus, String id){

        //子菜单
        List<TMenuDTO> childList = new ArrayList<>();
        for (TMenuDTO nav : menus) {
            // 遍历所有节点，将所有菜单的父id与传过来的根节点的id比较
            //相等说明：为该根节点的子节点。
            if(nav.getPId().equals(id)){
                childList.add(nav);
            }
        }
        //递归
        for (TMenuDTO nav : childList) {
            nav.setChild(getChild(menus, nav.getId()));
        }

        Collections.sort(new ArrayList<TMenuDTO>(childList), new Comparator<TMenuDTO>() {
            @Override
            public int compare(TMenuDTO o1, TMenuDTO o2) {
                return o1.getSeq().compareTo(o2.getSeq());
            }
        });//排序

        //如果节点下没有子节点，返回一个空List（递归退出）
        if(childList.size() == 0){
            return new ArrayList<TMenuDTO>();
        }
        return childList;
    }

}
