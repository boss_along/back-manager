package com.bosslong.backmanager.common.filter;

import com.bosslong.backmanager.common.constant.Constant;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @Author: boss_along
 * @Date: 2019/1/11 11:25
 * @Description:
 */
@Order(2)
///@WebFilter(urlPatterns = "/*",filterName = "ipblackFilter")
@Slf4j
public class IPBlackFilter implements Filter {

    @Autowired
    private StringRedisTemplate redisTemplate;

    private static final List<String> IGNOREURL = new ArrayList<>();

    static {
        IGNOREURL.add("/");
        IGNOREURL.add("/tologin");
        IGNOREURL.add("/static");
        IGNOREURL.add("/drawImage");
        //IGNOREURL.add("/static/*");

    }


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info(">>>>>>>>>>>>>>>>>>IPBlackFilter init...<<<<<<<<<<<<<<<<<<");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String token = request.getParameter(Constant.AUTHOR_TOKEN);

        String requestURI = request.getRequestURI();
        String[] rulString = requestURI.split("/");
        if(IGNOREURL.contains("/"+rulString[1])){
            filterChain.doFilter(servletRequest,servletResponse);
            return;
        }

        //token为空，提示用户重新登录
        String ip = ""+"_"+token;

        Long increment = redisTemplate.opsForValue().increment(ip, 1);
        if(1L==increment){
            redisTemplate.expire(ip,5, TimeUnit.MILLISECONDS);
        }
        if(30L<increment){
            response.sendRedirect(request.getContextPath()+"/tologin");
            return;
        }

        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {
        log.info(">>>>>>>>>>>>>>>>>>IPBlackFilter destroy...<<<<<<<<<<<<<<<<<<");
    }
}
