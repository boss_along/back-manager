package com.bosslong.backmanager.common.filter;

import com.bosslong.backmanager.common.constant.Constant;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: boss_along
 * @Date: 2019/1/11 13:08
 * @Description:
 */
@Order(1)
//@WebFilter(urlPatterns = "/*",filterName = "authorTokenFilter")
@Slf4j
public class AuthorTokenFilter implements Filter {

    private static final List<String> IGNOREURL = new ArrayList<>();

    static {
        IGNOREURL.add("/");
        IGNOREURL.add("/tologin");
        IGNOREURL.add("/static");
        IGNOREURL.add("/drawImage");
        //IGNOREURL.add("/static/*");

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info(">>>>>>>>>>>>>>>>>>AuthorTokenFilter init...<<<<<<<<<<<<<<<<<<");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        StringBuffer requestURL = request.getRequestURL();
        String requestURI = request.getRequestURI();

        String[] rulString = requestURI.split("/");

        if(IGNOREURL.contains("/"+rulString[1])){
            filterChain.doFilter(servletRequest,servletResponse);
            return;
        }

        String token = request.getParameter(Constant.AUTHOR_TOKEN);

        if(StringUtils.isBlank(token)){
            //request.getRequestDispatcher(request.getContextPath()+"/tologin").forward(request,response);
           response.sendRedirect(request.getContextPath()+"/tologin");
            return;
        }

        //filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {
        log.info(">>>>>>>>>>>>>>>>>>AuthorTokenFilter destroy...<<<<<<<<<<<<<<<<<<");
    }
}
