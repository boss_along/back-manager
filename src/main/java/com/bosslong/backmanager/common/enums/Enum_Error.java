package com.bosslong.backmanager.common.enums;

/**
 * @Author: boss_along
 * @Date: 2019/1/16 18:37
 * @Description:
 */
public enum Enum_Error {

    LOGIN_ERROR(20001,"用户名或密码错误!"),

    SERVER_ERROR(20002,"系统异常，请联系管理员!");

    /**
     *
     * @param errcode 错误码
     * @param errmsg 错误信息
     */
    Enum_Error(Integer errcode,String errmsg){
        this.errcode = errcode;
        this.errmsg = errmsg;
    }

    private Integer errcode;

    private String errmsg;

    public Integer getErrcode() {
        return errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

}
