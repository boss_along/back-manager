package com.bosslong.backmanager.common.interceptor;

import com.bosslong.backmanager.common.utils.IPUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

@Component
public class IPBlackInterceptor implements HandlerInterceptor {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String token = request.getParameter("token");

        String s = IPUtils.getrealIP(request);

        //token为空，提示用户重新登录
        String ip = ""+"_"+token;

        Long increment = redisTemplate.opsForValue().increment(ip, 1);
        if(1L==increment){
            redisTemplate.expire(ip,5, TimeUnit.MILLISECONDS);
        }
        if(30L<increment){
            response.sendRedirect(request.getContextPath()+"/login");
            return false;
        }
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
