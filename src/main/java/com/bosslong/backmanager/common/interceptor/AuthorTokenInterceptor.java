package com.bosslong.backmanager.common.interceptor;

import com.bosslong.backmanager.common.constant.Constant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author: boss_along
 * @Date: 2019/1/11 13:08
 * @Description:
 */
@Component
public class AuthorTokenInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String token = request.getParameter(Constant.AUTHOR_TOKEN);
        Cookie[] cookies = request.getCookies();


        for(int i=0 ;i<cookies.length; i++){
            Cookie cookie = cookies[i];
            boolean jsessionid = cookie.getName().equals("JSESSIONID");
            if(jsessionid){
                token = cookie.getValue();
            }
        }

        if(StringUtils.isBlank(token)){
            //request.getRequestDispatcher(request.getContextPath()+"/tologin").forward(request,response);
            response.sendRedirect(request.getContextPath()+"/");
            return false;
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
