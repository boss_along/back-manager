package com.bosslong.backmanager.common.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Thinkpad
 * @Title: CommonInterceptor
 * @ProjectName back-manager
 * @Description: TODO
 * @date 2019/1/2315:08
 * @Version: 1.0
 */
@Component
public class CommonInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String contextPath = request.getContextPath();
        request.setAttribute("ctx",contextPath);
        return true;
    }
}
