package com.bosslong.backmanager.entity;

import java.util.Date;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

/**
 * (TUser)实体类
 *
 * @author makejava
 * @since 2019-01-24 13:53:26
 */
@Data
public class TUserDTO implements Serializable {
    
    private static final long serialVersionUID = -60647242105671618L;    
            
    /**
    * 主键    
    */    
    private String id;   
            
    /**
    * 用户名    
    */    
    private String username;   
            
    /**
    * 密码    
    */    
    private String password;   
            
    /**
    * 真实姓名    
    */    
    private String trueName;   
            
    /**
    * 邮箱    
    */    
    private String email;   
            
    /**
    * 手机号    
    */    
    private String phone;   
            
    /**
    * 状态    
    */    
    private Integer status;   
            
    /**
    * 创建人    
    */    
    private String creater;   
            
    /**
    * 创建时间    
    */    
    private Date createTime;   
            
    /**
    * 修改人    
    */    
    private String modifer;   
            
    /**
    * 修改时间    
    */    
    private Date modifyTime;

    /**
     * 用户角色
     */
    @JsonIgnore
    private List<TRoleDTO> roles;

    /**
     * 用户菜单
     */
    @JsonIgnore
    private List<TMenuDTO> menus;
        
}