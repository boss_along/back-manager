package com.bosslong.backmanager.entity;

import java.util.Date;
import java.io.Serializable;
import lombok.Data;

/**
 * (TRoleMenu)实体类
 *
 * @author makejava
 * @since 2019-01-24 13:53:25
 */
@Data
public class TRoleMenuDTO implements Serializable {
    
    private static final long serialVersionUID = -38884518958259270L;    
            
    /**
    * 主键    
    */    
    private String id;   
            
    /**
    * 角色ID    
    */    
    private String roleId;   
            
    /**
    * 菜单ID    
    */    
    private String menuId;   
            
    /**
    * 状态    
    */    
    private Integer status;   
            
    /**
    * 创建人    
    */    
    private String creater;   
            
    /**
    * 创建时间    
    */    
    private Date createTime;   
            
    /**
    * 修改人    
    */    
    private String modifer;   
            
    /**
    * 修改时间    
    */    
    private Date modifyTime;   
        
}