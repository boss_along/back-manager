package com.bosslong.backmanager.entity;

import java.util.Date;
import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * (TUserRole)实体类
 *
 * @author makejava
 * @since 2019-01-24 13:53:26
 */
@Data
public class TUserRoleDTO implements Serializable {
    
    private static final long serialVersionUID = -78089211854482982L;    
            
    /**
    * 主键    
    */    
    private String id;   
            
    /**
    * 用户ID    
    */    
    private String userId;   
            
    /**
    * 角色ID    
    */    
    private String roleId;   
            
    /**
    * 状态    
    */    
    private Integer status;   
            
    /**
    * 创建人    
    */    
    private String creater;   
            
    /**
    * 创建时间    
    */    
    private Date createTime;   
            
    /**
    * 修改人    
    */    
    private String modifer;   
            
    /**
    * 修改时间    
    */    
    private Date modifyTime;
        
}