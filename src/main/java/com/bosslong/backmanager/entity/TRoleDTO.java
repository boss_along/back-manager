package com.bosslong.backmanager.entity;

import java.util.Date;
import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * (TRole)实体类
 *
 * @author makejava
 * @since 2019-01-24 13:53:25
 */
@Data
public class TRoleDTO implements Serializable {
    
    private static final long serialVersionUID = -39463100842547561L;    
            
    /**
    * 主键    
    */    
    private String id;   
            
    /**
    * 角色名称    
    */    
    private String bz;   
            
    /**
    * 角色描述    
    */    
    private String name;   
            
    /**
    * 状态    
    */    
    private Integer status;   
            
    /**
    * 创建人    
    */    
    private String creater;   
            
    /**
    * 创建时间    
    */    
    private Date createTime;   
            
    /**
    * 修改人    
    */    
    private String modifer;   
            
    /**
    * 修改时间    
    */    
    private Date modifyTime;

    /**
     * 用户菜单
     */
    private List<TMenuDTO> menus;
        
}