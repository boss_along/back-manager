package com.bosslong.backmanager.entity;

import java.util.Date;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import lombok.Data;

/**
 * (TMenu)实体类
 *
 * @author makejava
 * @since 2019-01-24 13:53:24
 */
@Data
public class TMenuDTO implements Serializable {
    
    private static final long serialVersionUID = 593090678046850280L;    
            
    /**
    * 主键    
    */    
    private String id;   
            
    /**
    * 菜单图标    
    */    
    private String icon;   
            
    /**
    * 菜单名称    
    */    
    private String name;   
            
    /**
    * 菜单等级    
    */    
    private Integer state;   
            
    /**
    * 菜单url    
    */    
    private String url;   
            
    /**
    * 上级菜单ID    
    */    
    private String pId;   
            
    /**
    * 是否叶子节点    
    */    
    private Integer isLeaf;   
            
    /**
    * 展示顺序    
    */    
    private Integer seq;   
            
    /**
    * 状态    
    */    
    private Integer status;   
            
    /**
    * 创建人    
    */    
    private String creater;   
            
    /**
    * 创建时间    
    */    
    private Date createTime;   
            
    /**
    * 修改人    
    */    
    private String modifer;   
            
    /**
    * 修改时间    
    */    
    private Date modifyTime;

    /**
     * 子菜单
     */
    private List<TMenuDTO> child;

}