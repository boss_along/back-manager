package com.bosslong.backmanager.controller;

import com.alibaba.fastjson.JSONObject;
import com.bosslong.backmanager.common.utils.UserInfo;
import com.bosslong.backmanager.entity.TMenuDTO;
import com.bosslong.backmanager.entity.TRoleDTO;
import com.bosslong.backmanager.entity.TUserDTO;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;



/**
 * 系统
 */
@Controller
public class IndexController {

    /**
     * 跳转到登录页面
     * @return
     */
    @RequestMapping("/")
    public String login(){
        return "login";
    }

    /**
     * 跳转到主页面
     * @return
     */
    @RequestMapping("/index")
    public String index(ModelMap modelMap){
        TUserDTO userInfo = UserInfo.getUserInfo();
        modelMap.addAttribute("loginUser", userInfo);
        modelMap.addAttribute("loginUsers", JSONObject.toJSONString(userInfo));
        return "main";
    }

    @RequestMapping("/inner")
    public String inner(ModelMap modelMap){
        return "inner";
    }
}
