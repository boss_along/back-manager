package com.bosslong.backmanager.controller;

import com.bosslong.backmanager.common.enums.Enum_Error;
import com.bosslong.backmanager.common.model.Result;
import com.bosslong.backmanager.entity.TMenuDTO;
import com.bosslong.backmanager.entity.TRoleDTO;
import com.bosslong.backmanager.entity.TUserDTO;
import com.bosslong.backmanager.service.TMenuService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * (TMenu)表控制层
 *
 * @author makejava
 * @since 2019-01-10 17:30:47
 */
@Controller
@RequestMapping("tMenu")
@Slf4j
public class TMenuController {
    /**
     * 服务对象
     */
    @Autowired
    private TMenuService tMenuService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    @ResponseBody
    public TMenuDTO selectOne(String id) {
        return this.tMenuService.queryById(id);
    }

    @GetMapping("/selectByPid/{pid}")
    @ResponseBody
    public Result selectByPid(@PathVariable(value = "pid") String pId){
        TUserDTO user = (TUserDTO)SecurityUtils.getSubject().getPrincipal();
        List<TRoleDTO> roles = user.getRoles();
        List<String> roleIds = new ArrayList<>();
        for (TRoleDTO tRole:roles) {
            roleIds.add(tRole.getId());
        }
        try {
            List<TMenuDTO> tMenus = tMenuService.queryByRoleIdsAndPid(roleIds,pId);
            Result ok = Result.ok(tMenus);
            return ok;
        } catch (Exception e) {
            log.error("com.bosslong.backmanager.controller.TMenuController.selectByPid error",e);
            return Result.error(Enum_Error.SERVER_ERROR.getErrcode(),Enum_Error.SERVER_ERROR.getErrmsg());
        }
    }
}