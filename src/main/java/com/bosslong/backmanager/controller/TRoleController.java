package com.bosslong.backmanager.controller;

import com.bosslong.backmanager.entity.TRoleDTO;
import com.bosslong.backmanager.service.TRoleService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (TRole)表控制层
 *
 * @author makejava
 * @since 2019-01-10 17:30:48
 */
@RestController
@RequestMapping("tRole")
public class TRoleController {
    /**
     * 服务对象
     */
    @Resource
    private TRoleService tRoleService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public TRoleDTO selectOne(String id) {
        return this.tRoleService.queryById(id);
    }

}