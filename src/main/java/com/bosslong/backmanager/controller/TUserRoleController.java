package com.bosslong.backmanager.controller;

import com.bosslong.backmanager.entity.TUserRoleDTO;
import com.bosslong.backmanager.service.TUserRoleService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (TUserRole)表控制层
 *
 * @author makejava
 * @since 2019-01-10 17:30:48
 */
@RestController
@RequestMapping("tUserRole")
public class TUserRoleController {
    /**
     * 服务对象
     */
    @Resource
    private TUserRoleService tUserRoleService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public TUserRoleDTO selectOne(String id) {
        return this.tUserRoleService.queryById(id);
    }

}