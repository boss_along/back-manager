package com.bosslong.backmanager.controller.user;

import com.alibaba.fastjson.JSONObject;
import com.bosslong.backmanager.common.enums.Enum_Error;
import com.bosslong.backmanager.common.model.Result;
import com.bosslong.backmanager.entity.TUserDTO;
import com.bosslong.backmanager.service.TUserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * (TUser)表控制层
 *
 * @author makejava
 * @since 2019-01-10 17:30:48
 */
@RequestMapping("/tUser")
@Controller
@Slf4j
public class TUserController {
    /**
     * 服务对象
     */
    @Resource
    private TUserService tUserService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public TUserDTO selectOne(String id) {
        return tUserService.queryById(id);
    }

    /**
     * 用户登陆
     *
     * @param tUser
     * @param response
     * @return
     */
    @RequestMapping("/login")
    @ResponseBody
    public Result selectByUsernameAndPassword(TUserDTO tUser, HttpServletResponse response) {

        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>用户登录参数:" + JSONObject.toJSONString(tUser) + "<<<<<<<<<<<<<<<<<<<<<<<<<<");
        Subject subject = SecurityUtils.getSubject();

        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(tUser.getUsername(), tUser.getPassword());

        //usernamePasswordToken.setRememberMe(true);
        try {
            subject.login(usernamePasswordToken);
            tUser = (TUserDTO) subject.getPrincipal();
            subject.getSession();
            return Result.ok(tUser);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(Enum_Error.LOGIN_ERROR.getErrcode(), Enum_Error.LOGIN_ERROR.getErrmsg());
        }
    }

    /**
     * 用户列表界面
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(ModelMap modelMap) {
        return "modules/user/list";
    }


    /**
     * 用户列表展示
     * @param tUser
     * @param page
     * @param limit
     * @return
     */
    @RequestMapping("/list")
    @ResponseBody
    public Result queryUsers(TUserDTO tUser, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit) {

        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>用户列表:" + JSONObject.toJSONString(tUser) + "<<<<<<<<<<<<<<<<<<<<<<<<<<");
        try {
            PageHelper.startPage(page, limit);
            List<TUserDTO> userDTOS = tUserService.query4Page(tUser);
            PageInfo info = new PageInfo(userDTOS);
            return Result.ok(info);
        } catch (Exception e) {
            log.error("用户列表分页查询异常",e);
            return Result.error(Enum_Error.LOGIN_ERROR.getErrcode(), Enum_Error.LOGIN_ERROR.getErrmsg());
        }
    }


    /**
     * 用户信息修改
     * @param tUser
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public Result update(TUserDTO tUser) {

        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>用户修改" + JSONObject.toJSONString(tUser) + "<<<<<<<<<<<<<<<<<<<<<<<<<<");
        try {
            tUserService.update(tUser);
            return Result.ok();
        } catch (Exception e) {
            log.error("用户信息更新异常",e);
            return Result.error(Enum_Error.LOGIN_ERROR.getErrcode(), Enum_Error.LOGIN_ERROR.getErrmsg());
        }
    }

    /**
     * 新增页面
     * @param modelMap
     * @return
     */
    @GetMapping("/toInsert")
    public String toInsert(ModelMap modelMap) {
        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>用户新增页面" + JSONObject.toJSONString("") + "<<<<<<<<<<<<<<<<<<<<<<<<<<");
       return "modules/user/add";
    }


    /**
     * 用户信息新增
     * @param tUser
     * @return
     */
    @RequestMapping("/insert")
    @ResponseBody
    public Result insert(TUserDTO tUser) {

        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>用户修改" + JSONObject.toJSONString(tUser) + "<<<<<<<<<<<<<<<<<<<<<<<<<<");
        try {
            tUserService.insert(tUser);
            return Result.ok();
        } catch (Exception e) {
            log.error("用户信息新增异常",e);
            return Result.error(Enum_Error.LOGIN_ERROR.getErrcode(), Enum_Error.LOGIN_ERROR.getErrmsg());
        }
    }


    /**
     *
     * @param id
     * @return
     */
    @GetMapping("/del/{id}")
    @ResponseBody
    public Result del(@PathVariable(value = "id") String id) {

        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>用户删除" + JSONObject.toJSONString(id) + "<<<<<<<<<<<<<<<<<<<<<<<<<<");
        try {
            tUserService.deleteById(id);
            return Result.ok();
        } catch (Exception e) {
            log.error("用户信息删除异常",e);
            return Result.error(Enum_Error.LOGIN_ERROR.getErrcode(), Enum_Error.LOGIN_ERROR.getErrmsg());
        }
    }

}