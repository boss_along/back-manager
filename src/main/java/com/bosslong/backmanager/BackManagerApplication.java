package com.bosslong.backmanager;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@ServletComponentScan("com.bosslong.backmanager.common")
@MapperScan("com.bosslong.backmanager.dao")
public class BackManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackManagerApplication.class, args);
    }

}

